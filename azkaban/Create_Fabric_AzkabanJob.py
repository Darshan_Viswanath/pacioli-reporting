__author__ = 'darshan.viswanath'

# copy the Example job file and replace text to create new job

import sys
import shutil
from zipfile import ZipFile

def copyfileobj(source, dest):
    shutil.copy(source, dest)


def replacetextinfile(destFileName, textToReplace):
    with open(destFileName, 'r') as file:
        filedata = file.read()

        # Replace the target string
        filedata = filedata.replace(textToReplace, jobName)

        # Write data to the same file
        with open(destFileName, 'w') as file:
            file.write(filedata)

#def zipfileforazkaban(zipFileName, destFileName):

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print 'Invalid number of arguments'
        sys.exit()

    if len(sys.argv) > 2:
        print 'Invalid number of arguments'
        sys.exit()

    jobName = sys.argv[1]
    destFileName = jobName + ".job"
    zipFileName = jobName + ".zip"
    textToReplace = 'Example'

# call copy file
copyfileobj("Example.job", destFileName)
print destFileName

# call replace text
replacetextinfile(destFileName, textToReplace)

# call zip file
#zipfileforazkaban(zipFileName, destFileName)
#print zipFileName

