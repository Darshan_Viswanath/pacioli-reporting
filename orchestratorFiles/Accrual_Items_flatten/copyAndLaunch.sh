#!/bin/sh
if [ "$#" -ne 4 ];
then
	echo "Requires hosts, json-config, spark-config, jobdir"
	exit
fi
config=$2
sparkconfig=$3
jobdir=$4
echo $jobdir
cd $jobdir
while read host
do
	echo $host Copying config and spark config
	scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem $jobdir/$config hadoop@$host:/tmp/
    scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem $jobdir/$sparkconfig hadoop@$host:/tmp/
    scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem $jobdir/AppConfig.xsd hadoop@$host:/tmp/
done < $1
sudo -u hadoop sh -x launchCommand.sh $2 $3 $jobdir