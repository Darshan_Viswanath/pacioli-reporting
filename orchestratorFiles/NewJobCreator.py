import os
import sys
from distutils.dir_util import copy_tree


class NewJobCreator:
    jobName = None
    def __init__(self, jobName):
        self.jobName = jobName;

    def createJobFiles(self, sourceFolder):
        filesCreated = False;
        self.createFolder(self.jobName)
        self.copyFiles(sourceFolder, self.jobName)
        self.replacePlaceholderName(self.jobName, sourceFolder, self.jobName)
        self.replaceXMLFileName(self.jobName, sourceFolder, self.jobName)

    def createFolder(self, jobName):
        created = False;
        if not os.path.exists(self.jobName):
            os.makedirs(self.jobName)
            created = True
        else:
            print("Path Exists.")
            created = False
        return created

    def copyFiles(self, sourceFolder, destinationFolder):
        copy_tree(sourceFolder, destinationFolder)

    def replacePlaceholderName(self, folder, placeholderJobName, jobName):
        for dname, dirs, files in os.walk(folder):
            for fname in files:
                fpath = os.path.join(dname, fname)
                with open(fpath) as f:
                    s = f.read()
                s = s.replace(placeholderJobName, jobName)
                with open(fpath, "w") as f:
                    f.write(s)

    def replaceXMLFileName(self, folder, placeholderJobName, jobName):
        extension = ".xml"
        os.rename(folder + "/" + placeholderJobName + extension, folder + "/" + jobName + extension)


if __name__=="__main__":
    jobName = sys.argv[1]
    newJobCreator = NewJobCreator(jobName);

    sourceFolder = "Example"
    newJobCreator.createJobFiles(sourceFolder);