#!/bin/sh
jobdir=$3
sleep $(( ( RANDOM % 60 )  + 1 ))
nohup spark-submit --name $1 --master yarn --deploy-mode cluster --class com.myntra.main.SparkMain --num-executors 20 --executor-memory 50G --driver-memory 6G --executor-cores 7 --conf '"spark.yarn.driver.memoryOverhead=4g"' --conf '"spark.yarn.executor.memoryOverhead=5g"' --conf '"spark.core.connection.ack.wait.timeout=1200"' --conf '"spark.memory.storageFraction=0.8"' --conf '"spark.app.name=Core_Invoices_flatten"' $jobdir/fabric.jar Core_Invoices_flatten /tmp/$1 /tmp/$2 /tmp/AppConfig.xsd >$jobdir/log.out 2>&1  < /dev/null &
