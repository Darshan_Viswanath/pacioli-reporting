#!/bin/sh
jobdir=$3
sleep $(( ( RANDOM % 60 )  + 1 ))
nohup spark-submit --name $1 --master yarn --deploy-mode cluster --class com.myntra.main.SparkMain --num-executors 48 --executor-memory 20G --driver-memory 100G --executor-cores 5 --conf '"spark.yarn.driver.memoryOverhead=4g"' --conf '"spark.yarn.executor.memoryOverhead=5g"' --conf '"spark.core.connection.ack.wait.timeout=1200"' --conf '"spark.memory.storageFraction=0.8"' --conf '"spark.app.name=ReversalRevenueInvoice_Packed_b2c_GST"' $jobdir/fabric.jar ReversalRevenueInvoice_Packed_b2c_GST /tmp/$1 /tmp/$2 /tmp/AppConfig.xsd >$jobdir/log.out 2>&1 < /dev/null &
