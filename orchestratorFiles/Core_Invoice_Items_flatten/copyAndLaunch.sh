#!/bin/sh
if [ "$#" -ne 4 ];
then
	echo "Requires hosts, json-config, spark-config, jobdir"
	exit
fi
config=$2
sparkconfig=$3
jobdir=$4
echo $jobdir
cd $jobdir
while read host
do
	echo $host Copying config and spark config
	scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem $jobdir/$config hadoop@$host:/tmp/
    scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem $jobdir/$sparkconfig hadoop@$host:/tmp/
    scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem $jobdir/AppConfig.xsd hadoop@$host:/tmp/
done < $1
#sudo -u hadoop sh -x launchCommand.sh $2 $3 $jobdir
echo nohup spark-submit --name $2 --master yarn --deploy-mode cluster --class com.myntra.main.SparkMain --num-executors 20 --executor-memory 50G --driver-memory 6G --executor-cores 7 --conf '"spark.yarn.driver.memoryOverhead=4g"' --conf '"spark.yarn.executor.memoryOverhead=5g"' --conf '"spark.core.connection.ack.wait.timeout=1200"' --conf '"spark.memory.storageFraction=0.8"' --conf '"spark.app.name=Core_Invoice_Items_flatten"' $jobdir/fabric.jar Core_Invoice_Items_flatten /tmp/$2 /tmp/$3 /tmp/AppConfig.xsd >$jobdir/log.out 2>&1 &
nohup spark-submit --name $2 --master yarn --deploy-mode cluster --class com.myntra.main.SparkMain --num-executors 20 --executor-memory 50G --driver-memory 6G --executor-cores 7 --conf '"spark.yarn.driver.memoryOverhead=4g"' --conf '"spark.yarn.executor.memoryOverhead=5g"' --conf '"spark.core.connection.ack.wait.timeout=1200"' --conf '"spark.memory.storageFraction=0.8"' --conf '"spark.app.name=Core_Invoice_Items_flatten"' $jobdir/fabric.jar Core_Invoice_Items_flatten /tmp/$2 /tmp/$3 /tmp/AppConfig.xsd >$jobdir/log.out 2>&1 &
