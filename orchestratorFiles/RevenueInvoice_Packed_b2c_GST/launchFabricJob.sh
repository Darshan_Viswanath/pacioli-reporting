#!/bin/sh
if [ "$#" -ne 9 ];
then
        echo "Requires master-ip, slaves, json-conf, spark-conf, copyAndLaunch script file, launch-script, jobjar, and start-time of the job"
        exit
fi
master=$1
hostfile=$2
jsonconfig=$3
sparkconfig=$4
copyScript=$5
launchScript=$6
jobjar=$7
jobdir=$8
basePath=$9

# Create job directory in all hosts.
# TODO: Change $jobdir to /mnt/flatten equivalent
#while read host
#do
#	echo ${host}
#	echo ssh -t -o StrictHostKeyChecking=no -i ~/bianalytics.pem hadoop@$host "sudo -u hadoop mkdir /mnt/personify"
#	ssh -t -o StrictHostKeyChecking=no -i ~/bianalytics.pem hadoop@$host "sudo -u hadoop mkdir /mnt/personify" || true
#done < $basePath$hostfile

> /home/myntra/.ssh/known_hosts

echo ssh -o StrictHostKeyChecking=no -i ~/bianalytics.pem -n hadoop@$master "'sudo -u hadoop mkdir $jobdir'"
ssh -t -o StrictHostKeyChecking=no -i ~/bianalytics.pem hadoop@$master "sudo -u hadoop mkdir -p $jobdir"

echo scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$hostfile hadoop@$master:/$jobdir
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$hostfile hadoop@$master:$jobdir

echo scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  ~/bianalytics.pem hadoop@$master:~/bianalytics.pem
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  ~/bianalytics.pem hadoop@$master:~/bianalytics.pem

echo scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$jsonconfig hadoop@$master:/$jobdir
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$jsonconfig hadoop@$master:$jobdir

echo scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$sparkconfig hadoop@$master:/$jobdir
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$sparkconfig hadoop@$master:$jobdir

echo scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$copyScript hadoop@$master:/$jobdir
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$copyScript hadoop@$master:$jobdir

echo scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$launchScript hadoop@$master:/$jobdir
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$launchScript hadoop@$master:$jobdir

echo scp -i ~/bianalytics.pem  $basePath/$jobjar hadoop@$master:/$jobdir
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/$jobjar hadoop@$master:$jobdir

echo scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/AppConfig.xsd hadoop@$master:$jobdir
scp -o StrictHostKeyChecking=no -i ~/bianalytics.pem  $basePath/AppConfig.xsd hadoop@$master:$jobdir

echo ssh -t -o StrictHostKeyChecking=no -i ~/bianalytics.pem -n hadoop@$master "'sudo -u hadoop sh $jobdir/$copyScript $hostfile $jsonconfig $sparkconfig $jobdir'"
ssh  -t -o StrictHostKeyChecking=no -i ~/bianalytics.pem hadoop@$master "sudo -u hadoop sh $jobdir/$copyScript $hostfile $jsonconfig $sparkconfig $jobdir"
