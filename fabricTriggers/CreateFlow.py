from S3Utils import S3
import sys

def startFlow(flowBasePath,jobName):
    import EmrLauncher
    resource = EmrLauncher.createEphemeralCluster(jobName + "Aggregates")
    clusterId = resource['JobFlowId']
    print "Cluster ID=%s" % clusterId
    EmrLauncher.waitForBootstrapping(clusterId)
    masterIp, allIps = EmrLauncher.getHostIPs(clusterId)
    allIps.insert(0,masterIp)
    print "Master and Task Nodes: "
    print "\n".join(allIps) + "\n"
    print "Cluster ID=%s" % clusterId
    s3Handler = S3()
    s3Handler.writeToS3(flowBasePath + "cluster", clusterId)

if __name__ == '__main__':
    basePath = "financialAccounting/"
    suffix = "AR"
    if len(sys.argv) >= 2:
        suffix = sys.argv[1]
    flowBasePath = basePath + suffix + "/"
    startFlow(flowBasePath, suffix)
