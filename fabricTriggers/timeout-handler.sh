sysctl -w "net.ipv4.conf.all.rp_filter=1"
sysctl -w "net.ipv4.tcp_max_tw_buckets=1440000"
sysctl -w "net.ipv4.ip_local_port_range=1024 65535"
sysctl -w "net.ipv4.tcp_max_syn_backlog=65536"
sysctl -w "net.core.rmem_max=67108864"
sysctl -w "net.core.wmem_max=67108864"
sysctl -w "net.ipv4.tcp_rmem=4096 87380 33554432"
sysctl -w "net.ipv4.tcp_wmem=4096 65536 33554432"
sysctl -w "net.core.somaxconn=65534"
sysctl -w "net.ipv4.tcp_fin_timeout=30"
sysctl -w "net.ipv4.tcp_tw_reuse=1"
sysctl -w "net.ipv4.udp_rmem_min=131072"
sync; echo 10 > /proc/sys/vm/swappiness
service network restart
