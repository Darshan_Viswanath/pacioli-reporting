import datetime
import json
from xml.etree.ElementTree import Element, SubElement, tostring
import re
import time


# returns config value from sys arg
def getPropertyValue(config, expectedField):
    inputField = config.split("=")[0]
    if inputField == expectedField:
        return config.split("=")[1]
    else:
        raise ValueError(
            "expected field does not match input field. expected: " + expectedField + " , input: " + inputField)


def getTemplateString(timestamp, template):
    dateTime = datetime.datetime.fromtimestamp(
        long(timestamp)
    ).strftime('%Y-%m-%d %H:%M:%S')
    date = dateTime.split(" ")[0]
    time = dateTime.split(" ")[1]
    year = date.split("-")[0]
    month = date.split("-")[1]
    day = date.split("-")[2]
    hour = time.split(":")[0]
    minutes = time.split(":")[1]
    timedTemplate = template % dict(y=year, m=month, d=day, h=hour, min=minutes)
    return timedTemplate


# returns config value from config file
def getFromConfig(s3handler, fieldName, configS3Path):
    data = json.loads(s3handler.readFromS3(configS3Path))
    return data[fieldName]

def getTimeShift(timeShiftList):
    timeShiftMap = {}
    timeShiftRequiredMap = {}
    keyMap = {'d':'days', 'h':'hours', 'min':'minutes'}
    signMap = { '-' : -1, '+' : 1, '' : -1}
    for timeShift in timeShiftList:
        timeShiftMap[timeShift.group(1)] = 0 if timeShift.group(3) == '' else int(timeShift.group(3)) * signMap[timeShift.group(2)]
    for key in keyMap:
        replacementKey = keyMap[key]
        if key in timeShiftMap.keys():
            timeShiftRequiredMap[replacementKey] = timeShiftMap.pop(key)
    timeShiftInSec = datetime.timedelta(**timeShiftRequiredMap).total_seconds()
    return timeShiftInSec

# returns 'datetime' component of the path
def getTimePath(timestamp):
    dateTime = datetime.datetime.fromtimestamp(
        long(timestamp)
    ).strftime('%Y-%m-%d %H:%M:%S')
    date = dateTime.split(" ")[0]
    time = dateTime.split(" ")[1]
    year = date.split("-")[0]
    month = date.split("-")[1]
    day = date.split("-")[2]
    hour = time.split(":")[0]
    minutes = time.split(":")[1]

    return {'y': year, 'm': month, 'd': day, 'h': hour, 'min': minutes}


def getInterval(intervalList, timeValue):
    for i in intervalList:
        start = i[0]
        end = i[1]
        if long(start) <= long(timeValue) < long(end):
            return start

def getTimePathList(binInterval, currentJobTime, endTime, timeShift = 0):
    currentJobTime = str(long(currentJobTime) + timeShift)
    endTime = str(long(endTime) + timeShift)
    timePathList = []
    if binInterval.startswith("minutes"):
        timePeriod = binInterval.split(",")[1]
        fromTime = "00"
        toTime = timePeriod
        intervals = []
        while int(toTime) <= 60:
            if len(fromTime) < 2:
                fromTime = "0" + fromTime
            if len(toTime) < 2:
                toTime = "0" + toTime
            intervals += [(fromTime, toTime)]
            fromTime = toTime
            toTime = str(int(toTime) + int(timePeriod))
        while long(currentJobTime) < long(endTime):
            timePath = getTimePath(str(currentJobTime))
            timePath["min"] = getInterval(intervals, timePath["min"])
            currentJobTime = long(currentJobTime) + long(timePeriod) * 60
            timePathList += [timePath]
    elif binInterval.startswith("hours"):
        timePeriod = binInterval.split(",")[1]
        fromTime = "00"
        toTime = timePeriod
        intervals = []
        while int(toTime) <= 24:
            if len(fromTime) < 2:
                fromTime = "0" + fromTime
            if len(toTime) < 2:
                toTime = "0" + toTime
            intervals += [(fromTime, toTime)]
            fromTime = toTime
            toTime = str(int(toTime) + int(timePeriod))
        while long(currentJobTime) < long(endTime):
            timePath = getTimePath(str(currentJobTime))
            timePath["h"] = getInterval(intervals, timePath["h"])
            currentJobTime = long(currentJobTime) + long(timePeriod) * 60 * 60
            timePathList += [timePath]
    # TO_DO : else block
    return timePathList

def checkInputArguments(conf):
    compulsoryArguments = set(["startTime", "binInterval", "jobInterval", "master", "template", "s3JobPath"])
    missingArguments = compulsoryArguments - set(conf.keys())
    if len(missingArguments) > 0:
        print "Missing arguments: %s" % (str(list(missingArguments)))
        exit(1)


def getJobExecTimes(s3handler, conf, successTimestampS3Path=None):
    successTimestamp = s3handler.readFromS3(successTimestampS3Path) if successTimestampS3Path is not None else 0
    jobStartTime = conf["startTime"]
    if long(jobStartTime) <= long(successTimestamp):
        jobStartTime = str(long(successTimestamp) + 1800)
    endTime = long(jobStartTime) + long(conf["jobInterval"])
    currentJobTime = jobStartTime
    print "jobStartTime - " + jobStartTime
    return jobStartTime, currentJobTime, endTime


def populatePathTemplates(cubes, filePathsList, binInterval, currentJobTime, endTime, s3handler):
    namespaces = {'fabric':'http://www.myntra.com/fabric'}
    pathAvailable = True

    for cube in cubes:
        pathSet = []
        paths = cube.find('fabric:paths',namespaces)
        cubeType = cube.find('fabric:cubeType',namespaces).text
        if cubeType in ['source', 'sink'] and paths.find('fabric:path',namespaces) is not None:
            for pathTemplate in paths.findall('fabric:path',namespaces):
                if re.search(r"%\(\w+\)s", pathTemplate.text):
                    paths.remove(pathTemplate)
                    template = pathTemplate.text
                    timeShiftList = list(re.finditer(r"(\w+?)=([-+]?)([0-9]*?)%",template))
                    template = re.sub(r"(\w+?)=([-+]?[0-9]*?)%(.*?)/",r'\1=%\3/',template)
                    if len(timeShiftList) != 0:
                        timeShiftInSec = long(getTimeShift(timeShiftList))
                        filePathsList = getTimePathList(binInterval, currentJobTime, endTime, timeShiftInSec)
                    else:
                        filePathsList = getTimePathList(binInterval, currentJobTime, endTime)
                    for i in range(len(filePathsList)):
                        if binInterval.startswith("minutes"):
                            path = template % filePathsList[i]
                            if cubeType=='source':
                                pathAvailable &= s3handler.fileExistsInS3(path.split("/")[0], "/".join(path.split("/")[1:]))
                                if path not in pathSet:
                                    pathSet += [path]
                                    pathElem = SubElement(paths, "path")
                                    pathElem.text = path
                            if cubeType == 'sink':
                                if path not in pathSet:
                                    pathSet += [path]
                                    pathElem = SubElement(paths, "path")
                                    pathElem.text = path
    return pathAvailable

def replaceConstantInXML(xmlstring, constants, values):
    for constant, value in zip(constants, values):
        print constant,value
        pattern = re.compile(r'\${' + constant + '}')
        xmlstring = re.sub(pattern,str(value),xmlstring)
    return xmlstring

def getStartTimestampOfDay():
    temp = time.time()
    return temp - (temp % 86400)


