#!/usr/bin/python
import sys
import time
import subprocess
import redis
import lxml.etree as etree
from S3Utils import S3
from AbstractFlow import Flow
import utils


class FlattenOrchestratorFlow(Flow):
    def prepare(self):
        # TODO: Make this config driven
        s3DataTemplate = "y=%(y)s/m=%(m)s/d=%(d)s/h=%(h)s/min=%(min)s/"
        s3TimedPath = utils.getTemplateString(self.config["jobStartTime"], s3DataTemplate)
        if self.config["s3JobPath"].startswith('eventsFlatten'):
            s3DataPath = "flattenedData/data=eventsTable/" + s3TimedPath
            s3OrcDataPath = "flattenedDataOrc/data=eventsTable/" + s3TimedPath
            self.s3handler.deleteDirectory(s3DataPath)
            self.s3handler.deleteDirectory(s3OrcDataPath)
        elif self.config["s3JobPath"].startswith('widgetsFlatten'):
            s3DataPath = "flattenedData/data=widgetsTable/" + s3TimedPath
            s3OrcDataPath = "flattenedDataOrc/data=widgetsTable/" + s3TimedPath
            self.s3handler.deleteDirectory(s3DataPath)
            self.s3handler.deleteDirectory(s3OrcDataPath)
        elif self.config["s3JobPath"].startswith('usersFlatten'):
            s3DataPath = "flattenedData/data=usersTable/" + s3TimedPath
            s3OrcDataPath = "flattenedDataOrc/data=usersTable/" + s3TimedPath
            self.s3handler.deleteDirectory(s3DataPath)
            self.s3handler.deleteDirectory(s3OrcDataPath)

    def downloadDependentFiles(self, filesList):
        """filesList is a list[tuple(s3path, localPath)]"""
        for files in filesList:
            self.s3handler.downloadFromS3(files[0], files[1])

    def createJobConfig(self):
        timePathList = utils.getTimePathList(opts["binInterval"], self.config["currentJobTime"], self.config["endTime"])
        eventsList = opts["eventsType"].split(',')
        filePathsList = []
        for i in range(len(eventsList)):
            for timePath in timePathList:
                timePath["type"] = [eventsList[i]]
                filePathsList += [timePath]  # TODO: Not required
        xmlTemplate = etree.parse(self.config["xmlTemplatePath"])
        templateRoot = xmlTemplate.getroot()
        cubes = templateRoot.find('Cubes').findall('Cube')
        utils.populatePathTemplates(cubes, filePathsList, opts["binInterval"], self.s3handler)
        jobConfigFile = opts["template"].split(".")[0] + "_" + str(self.config["jobStartTime"]) + ".xml"
        xmlTemplate.write(self.config["basePath"] + jobConfigFile)
        return self.config["basePath"] + jobConfigFile

    def run(self):
        subprocess.call(['bash', basePath + 'launchFabricJob.sh', self.config["master"],
                         self.config["hostsPath"], self.config["jobConfigFile"], self.config["sparkConfPath"],
                         self.config["copyScript"],
                         basePath+self.config["launchScript"], self.config["jar"], self.config["jobDirectory"],
                         self.config["basePath"]])

    def pollForSuccess(self):
        appId = "-"
        while True:
            time.sleep(10)
            res = subprocess.check_output(
                "ssh -i /home/myntra/bianalytics.pem hadoop@" + self.config["master"]
                + " yarn application -list | grep " + self.config["jobConfigFile"]
                + " | awk -F\  '{print $1, $6, $9}'",
                shell=True)
            if len(res) > 0:
                appId = res.split(" ")[0]
                break
        print appId

        finalState = "NOT STARTED"
        while finalState not in ("SUCCEEDED", "FAILED"):
            time.sleep(60)
            finalStatePrefix = subprocess.check_output(
                "ssh -i /home/myntra/bianalytics.pem hadoop@" + self.config["master"]
                + " yarn application -status " + appId
                + "   | grep Final-State",
                shell=True)
            finalState = finalStatePrefix.split(":")[1].strip()
            print "status - " + finalState

            hostContent = self.s3handler.readFromS3(self.config["s3JobPath"] + self.config["hostsPath"])
            sz = len(hostContent.split("\n"))
            for i in range(sz - 1):
                hostExec = hostContent.split("\n")[i]
                hostLogin = "hadoop@" + hostExec
                print "removing /mnt/flatten from " + hostLogin
                subprocess.call(
                    ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/home/myntra/bianalytics.pem', hostLogin, 'sudo',
                     '-u',
                     'root', 'rm', '-r', '/mnt/flatten/*'])

            print "deleting jar from Spark master..."
            masterLogin = "hadoop@" + self.config["master"]
            jarPath = self.config["jobDirectory"] + self.config["jar"]
            subprocess.call(
                ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/home/myntra/bianalytics.pem',
                 masterLogin, 'sudo', '-u', 'root', 'rm', '-r', jarPath]
            )

            if finalState == "SUCCEEDED":
                print "Spark Job Succeeded."
                redis_conn = redis.StrictRedis(host="10.0.0.247", port=int(6379), db=0)
                key = self.config["s3JobPath"]
                if self.config["s3JobPath"].startswith('usersFlatten'):
                    templateContent = "y=%(y)s/m=%(m)s/d=%(d)s/h=%(h)s/min=%(min)s"
                    redisTemplate = utils.getTemplateString(jobStartTime, templateContent)
                    key = "flatten_events"
                    redis_conn.sadd(key, redisTemplate)
                    print "added " + redisTemplate + " to redis at key: " + key
                    key = "flatten_widget_entity"
                    redis_conn.sadd(key, redisTemplate)
                    print "added " + redisTemplate + " to redis at key: " + key
                    key = "flatten_event_userdata"
                    redis_conn.sadd(key, redisTemplate)
                    print "added " + redisTemplate + " to redis at key: " + key
                    self.s3handler.writeToS3(successTimestampS3Path, jobStartTime)
                print "Flow Succeeded."

            else:
                print "Job Failed."
                sys.exit(1)

    def complete(self):
        pass

    def execute(self):
        super(FlattenOrchestratorFlow, self).execute()


if __name__ == '__main__':
    import getopt

    s3Handler = S3()

    opts, args = getopt.getopt(s3Handler.readFromS3("flattenJob/defaultConfig.conf"), "s:b:j:e:m:t:p:j:",
                               ["startTime=", "binInterval=", "jobInterval=", "eventsType=", "master=", "template=", "s3JobPath="])
    utils.checkInputArguments(opts)
    successTimestampS3Path = "flattenJob/lastSuccessTime"
    jobStartTime, currentJobTime, endTime = utils.getJobExecTimes(s3Handler, opts, successTimestampS3Path)

    basePath = "/mnt/fabric/FabricOrchestration/Personify/"
    config = {
        "s3JobPath": opts["s3JobPath"],
        "downloadFileList": [
            ((opts["s3JobPath"] + opts["template"]), (basePath + opts["template"])),
            ((opts["s3JobPath"] + "launchFabricJob.sh"), basePath + "launchFabricJob.sh"),
            ((opts["s3JobPath"] + "copyAndLaunch.sh"), basePath + "copyAndLaunch.sh"),
            ((opts["s3JobPath"] + "hosts"), basePath + "hosts"),
            ((opts["s3JobPath"] + "launchCommand.sh"), basePath + "launchCommand.sh"),
            ((opts["s3JobPath"] + "spark-config.properties"), basePath + "spark-config.properties"),
            ((opts["s3JobPath"] + "fabric-0.1.1-SNAPSHOT-jar-with-dependencies.jar"), basePath + "fabric.jar")
        ],
        "xmlTemplatePath": basePath + opts["template"],
        "master": opts["master"],
        "sparkConfPath": "spark-config.properties",
        "copyScript": "copyAndLaunch.sh",
        "launchScript": "launchCommand.sh",
        "jar": "fabric.jar",
        "jobDirectory": "/home/hadoop/" + jobStartTime + "_" + opts["s3JobPath"],
        "hostsPath": "hosts",
        "basePath": basePath,
        "jobStartTime": jobStartTime,
        "currentJobTime": currentJobTime,
        "endTime": endTime
    }

    jobHandler = FlattenOrchestratorFlow(config)
    jobHandler.execute()
