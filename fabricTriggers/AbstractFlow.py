import abc
from S3Utils import S3

class Flow:
    __metaclass__ = abc.ABCMeta

    def __init__(self, config):
        self.s3handler = S3()
        self.config = config
        self.finalState = "Not Started"
        self.exitStatus = 0

    @abc.abstractmethod
    def prepare(self):
        """Set up runtime env, etc"""
        return

    @abc.abstractmethod
    def downloadDependentFiles(self, filesList):
        """Download all files from S3 on which the job is dependent."""
        return

    @abc.abstractmethod
    def createJobConfig(self):
        """Parse the XML, generate the paths and return the final job config"""
        return

    @abc.abstractmethod
    def run(self):
        """Run commands for spark job execution"""
        return

    @abc.abstractmethod
    def pollForSuccess(self):
        """Check the job status"""
        return

    @abc.abstractmethod
    def complete(self):
        """Delete the files not needed and update the success status."""
        return

    @abc.abstractmethod
    def execute(self):
        """
            Call all steps here. Generic ones are:
            1. Prepare (Set up env, etc.)
            2. Download files
            3. Create Job Config
            4. Execute the job
            5. Poll for Success
            6. Complete
        """
        self.prepare()
        self.downloadDependentFiles(self.config["downloadFileList"])
        self.config["jobConfigFile"] = self.createJobConfig()
        self.run()
        self.pollForSuccess()
        self.exitStatus = self.complete()
