import sys

import utils
from PersonifyOrchestrator import PersonifyOrchestratorFlow
from S3Utils import S3

if __name__ == '__main__':
    import re
    import EmrLauncher

    defaultConf = "userDate/defaultConfig.conf"
    s3Handler = S3()
    inp = re.split(r"\s+", s3Handler.readFromS3(defaultConf)) if len(sys.argv) < 2 \
        else sys.argv[1:]
    opts = {x.split("=")[0]: x.split("=")[1] for x in inp}
    print opts
    utils.checkInputArguments(opts)

    resource = EmrLauncher.createEphemeralCluster("UserDateAggregates")
    clusterId = resource['JobFlowId']
    print "Cluster ID=%s" % clusterId
    EmrLauncher.waitForBootstrapping(clusterId)
    masterIp, allIps = EmrLauncher.getHostIPs(clusterId)
    opts["master"] = masterIp
    s3Handler.writeToS3(opts["s3JobPath"] + "hosts", "\n".join(allIps) + "\n")

    jobStartTime = currentJobTime = opts["startTime"]
    endTime = long(jobStartTime) + long(opts["jobInterval"])
    basePath = "/mnt/fabric/FabricOrchestration/UserDate/"
    config = {
        "s3JobPath": opts["s3JobPath"],
        "downloadFileList": [
            ((opts["s3JobPath"] + opts["template"]), (basePath + opts["template"])),
            ((opts["s3JobPath"] + "launchFabricJob.sh"), basePath + "launchFabricJob.sh"),
            ((opts["s3JobPath"] + "copyAndLaunch.sh"), basePath + "copyAndLaunch.sh"),
            ((opts["s3JobPath"] + "hosts"), basePath + "hosts"),
            ((opts["s3JobPath"] + "launchCommand.sh"), basePath + "launchCommand.sh"),
            ((opts["s3JobPath"] + "spark-config.properties"), basePath + "spark-config.properties"),
            ((opts["s3JobPath"] + "fabric-0.1.1-SNAPSHOT-jar-with-dependencies.jar"), basePath + "fabric.jar"),
            ((opts["s3JobPath"] + "AppConfig.xsd"), basePath + "AppConfig.xsd")
        ],
        "xmlTemplatePath": basePath + opts["template"],
        "master": opts["master"],
        "jobStartTime": opts["startTime"],
        "sparkConfPath": "spark-config.properties",
        "copyScript": "copyAndLaunch.sh",
        "launchScript": "launchCommand.sh",
        "jar": "fabric.jar",
        "jobDirectory": "/home/hadoop/" + opts["startTime"] + "_" + opts["s3JobPath"],
        "hostsPath": "hosts",
        "basePath": basePath,
        "currentJobTime": currentJobTime,
        "launchFabricJob": "launchFabricJob.sh",
        "endTime": endTime,
        "defaultConfLocation": defaultConf
    }

    jobHandler = PersonifyOrchestratorFlow(config)
    jobHandler.setOpt(opts)
    jobHandler.execute()

    EmrLauncher.terminateCluster(clusterId)
