import boto3, smtplib, time, sys


def send_email(user, pwd, recipient, subject, body):
    gmail_user = user
    gmail_pwd = pwd
    FROM = user
    TO = recipient if type(recipient) is list else [recipient]
    SUBJECT = subject
    TEXT = body

    # Prepare actual message
    message = """\From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (FROM, ", ".join(TO), SUBJECT, TEXT)
    try:
        server = smtplib.SMTP("smtp.gmail.com", 587)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(FROM, TO, message)
        server.close()
        print 'successfully sent the mail'
    except:
        print "failed to send mail"


def createEphemeralCluster(clusterName):
    client = boto3.client('emr', region_name='ap-southeast-1')

    response = client.run_job_flow(
        Name=clusterName,
        LogUri='s3://atlas-ng-secor/fabric/emrlauncher/logs/',
        ReleaseLabel='emr-5.2.1',
        Instances={
            'MasterInstanceType': 'm4.10xlarge',
            'SlaveInstanceType': 'r3.8xlarge',
            'InstanceCount': 13,
            'Ec2KeyName': 'bianalytics',
            'KeepJobFlowAliveWhenNoSteps': True,
            'TerminationProtected': False,
            'Ec2SubnetId': 'subnet-987236f1',
            'EmrManagedMasterSecurityGroup': 'sg-5b7d6237',
            'EmrManagedSlaveSecurityGroup': 'sg-547d6238'
        },
        Applications=[
            {
                'Name': 'Spark'
            }
        ],
        # Steps= step,
        VisibleToAllUsers=True,
        JobFlowRole='EMR_EC2_DefaultRole',
        ServiceRole='EMR_DefaultRole',
        Tags=[
            {
                'Key': 'Owner',
                'Value': 'Bhanu'
            },
            {
                'Key': 'Team',
                'Value': 'Atlas'
            },
            {
                'Key': 'Dept',
                'Value': 'BI'
            },
            {
                'Key': 'Service',
                'Value': 'awsatlas'
            }
        ]
    )
    return response


def waitForBootstrapping(clusterId):
    client = boto3.client('emr', region_name='ap-southeast-1')
    state = 'STARTING'
    while state in ['STARTING', 'BOOTSTRAPPING']:
        response = client.describe_cluster(
            ClusterId=clusterId
        )
        state = response['Cluster']['Status']['State']
        print state

        if state == 'WAITING':
            print "Cluster ready"
            return

        time.sleep(60)

    print "Wrong state reached"
    sys.exit(1)


def terminateCluster(clusterId):
    client = boto3.client('emr', region_name='ap-southeast-1')
    response = client.terminate_job_flows(
        JobFlowIds=[
            clusterId
        ]
    )

    state = ''
    while state != 'TERMINATING' or state != 'TERMINATED':
        response = client.describe_cluster(
            ClusterId=clusterId
        )
        state = response['Cluster']['Status']['State']
        print state

        if state == 'TERMINATING' or state == 'TERMINATED':
            return

        time.sleep(60)


def getHostIPs(clusterId):
    """Returns master, all ips"""
    client = boto3.client('emr', region_name='ap-southeast-1')
    print clusterId
    response = client.list_instances(ClusterId=clusterId)
    ips = []
    for instance in response['Instances']:
        ips.append(instance['PublicDnsName'])
    response = client.list_instances(ClusterId=clusterId, InstanceGroupTypes=['MASTER'])
    return response['Instances'][0]['PublicDnsName'], ips


if __name__ == '__main__':
    resource = createEphemeralCluster()
    clusterId = resource['JobFlowId']
    print "Cluster ID=%s" % clusterId
    waitForBootstrapping(clusterId)
    time.sleep(60)
    terminateCluster(clusterId)
