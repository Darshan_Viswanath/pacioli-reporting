from S3Utils import S3
import sys

def endFlow(flowBasePath):
    import EmrLauncher
    s3Handler = S3()
    clusterPath = flowBasePath + "cluster"
    clusterId = s3Handler.readFromS3(clusterPath)
    EmrLauncher.terminateCluster(clusterId)

if __name__  == '__main__':
    basePath = "financialAccounting/"
    suffix = "AR"
    if len(sys.argv) >= 2:
        suffix = sys.argv[1]
    flowBasePath = basePath + suffix + "/"
    endFlow(flowBasePath)
