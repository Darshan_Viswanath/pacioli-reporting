#!/bin/sh

slackURL="https://hooks.slack.com/services/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"

publishChannel="#fin_acc_monitoring"
processName="Pacioli Data Processing"

  publishMessage="\`Pacioli Data Processing completed \`"
  curl -H "Content-Type: application/json" -X POST -d "{'text':'$publishMessage', 'channel': '$publishChannel', 'link_names': 1}" $slackURL
