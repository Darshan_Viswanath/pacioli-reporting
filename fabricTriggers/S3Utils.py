import boto3
import botocore
import time
import sys


class S3:
    def __init__(self, aws_access_key='AKIAJGMIZKV272KTDEYQ',
                 aws_secret_key='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'):
        self.s3 = boto3.client('s3', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key)
        self.resource = boto3.resource('s3', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key)

    def readFromS3(self, s3file, bucket="myntra-conf"):
        try:
            obj = self.resource.Object(bucket, s3file)
            content = obj.get()['Body'].read()
            print "read " + s3file + " from S3"
            return str(content)
        except botocore.exceptions.ClientError as e:
            print "Error while reading " + s3file + " from S3. " + e.message
            return "0"

    def downloadFromS3(self, s3path, localPath, bucket="myntra-conf"):
        try:
            self.s3.download_file(bucket, s3path, localPath)
            print "successfully downloaded: %s" % s3path
        except botocore.exceptions.ClientError as e:
            print "Error while downloading " + s3path + " from S3."

    def writeToS3(self, s3file, content, bucket="myntra-conf"):
        try:
            obj = self.resource.Object(bucket, s3file)
            obj.put(Body=str(content))
            print "successfully written " + content + " to S3."
        except botocore.exceptions.ClientError as e:
            print "Error while writing " + content + " to S3. " + e.message

    def deleteDirectory(self, s3DataPath, bucket="madlytics"):
        try:
            objects_to_delete = self.resource.meta.client.list_objects(Bucket=bucket, Prefix=s3DataPath)
            delete_keys = {
                'Objects': [{'Key': k} for k in [obj['Key'] for obj in objects_to_delete.get('Contents', [])]]}
            self.resource.meta.client.delete_objects(Bucket=bucket, Delete=delete_keys)
            print "Successfully deleted " + s3DataPath + " from S3."
        except botocore.exceptions.ClientError as e:
            print "Error while deleting " + s3DataPath + " from S3. " + e.message

    def fileExistsInS3(self, bucket, s3File):
        res = self.s3.list_objects(Bucket=bucket, Prefix=s3File)
        content = res.get('Contents')
        if content is None:
            print s3File + " path does not exist."
            return False
        else:
            return True

    def existsInS3(self, bucket, s3File):
        try:
            res = self.s3.list_objects(Bucket=bucket, Prefix=s3File)
            content = res.get('Contents')
            if content is None:
                print s3File + " path does not exist."
                return False
            else:
                print s3File + " exists. Checking for SUCCESS file."
                timer = 0
                timeout = 3600
                while True:
                    successFilePath = "/".join(s3File.split("/")[0:7]) + "/_SUCCESS"
                    success = self.s3.list_objects(Bucket=bucket, Prefix=successFilePath)
                    successContent = success.get('Contents')
                    if successContent is None:
                        if timer >= timeout:
                            print "Timeout occurred while waiting for SUCCESS file " + successFilePath
                            sys.exit(1)
                        print "Waiting for SUCCESS file - " + successFilePath
                        time.sleep(1)
                        timer += 1
                    else:
                        print "Found SUCCESS file"
                        return True

        except botocore.exceptions.ClientError as e:
            print e.message
            return False
