#!/usr/bin/python
import sys
import time
import subprocess
import lxml.etree as etree
from S3Utils import S3
from AbstractFlow import Flow
import utils


class PersonifyOrchestratorFlow(Flow):
    def setOpt(self, opts):
        self.opts = opts

    def prepare(self):
        hostContent = self.s3handler.readFromS3(self.config["s3JobPath"] + self.config["hostsPath"])
        for hostExec in hostContent.strip().split("\n"):
            # hostExec = hostContent.split("\n")[i]
            hostLogin = "hadoop@" + hostExec
            print "mkdir /mnt/personify in " + hostLogin

            print subprocess.check_output(
                ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/home/myntra/bianalytics.pem', hostLogin, 'sudo',
                 '-u',
                 'hadoop', 'mkdir', '-p', '/mnt/personify/'])
            print subprocess.check_output(
                ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/home/myntra/bianalytics.pem', hostLogin, "sudo",
                 "chown", "-R", "yarn:yarn", "/mnt/personify"])

    def downloadDependentFiles(self, filesList):
        """filesList is a list[tuple(s3path, localPath)]"""
        for files in filesList:
            self.s3handler.downloadFromS3(files[0], files[1])

    def createJobConfig(self):
        namespaces = {'fabric':'http://www.myntra.com/fabric'}
        timePathList = utils.getTimePathList(self.opts["binInterval"], self.config["currentJobTime"], self.config["endTime"])
        xmlTemplate = etree.parse(self.config["xmlTemplatePath"])
        templateRoot = xmlTemplate.getroot()
        cubes = templateRoot.find('fabric:Cubes',namespaces).findall('fabric:Cube',namespaces)
        utils.populatePathTemplates(cubes, timePathList, self.opts["binInterval"], self.config["currentJobTime"], self.config["endTime"], self.s3handler)
        jobConfigFile = self.opts["template"].split(".")[0] + "_" + str(self.config["jobStartTime"]) + ".xml"
        xmlTemplate.write(self.config["basePath"] + jobConfigFile)
        if self.opts["filterReplacementEnabled"] == 'True' or self.opts["filterReplacementEnabled"] == 'true':
            startTime = self.opts["startTime"]
            FILTER_START_CONST = "filterStartTime"
            FILTER_END_CONST = "filterEndTime"
            filterStartTime = long(startTime) - 5356800
            filterEndTime = long(startTime) + 86400
            xmlAsString = open(self.config["basePath"] + jobConfigFile, 'r').read()
            xmlAsString = utils.replaceConstantInXML(xmlAsString, [FILTER_START_CONST,FILTER_END_CONST], [filterStartTime,filterEndTime])
            open(self.config["basePath"] + jobConfigFile,'w').write(xmlAsString)
        return jobConfigFile

    def run(self):
        subprocess.call(['bash', self.config["basePath"] + self.config["launchFabricJob"], self.config["master"],
                         self.config["hostsPath"], self.config["jobConfigFile"], self.config["sparkConfPath"],
                         self.config["copyScript"],
                         self.config["launchScript"], self.config["jar"], self.config["jobDirectory"],
                         self.config["basePath"]])

    def pollForSuccess(self):
        appId = "-"
        while True:
            time.sleep(10)
            res = subprocess.check_output(
                "ssh -o StrictHostKeyChecking=no -i /home/myntra/bianalytics.pem hadoop@" + self.config["master"]
                + " yarn application -list | grep " + self.config["jobConfigFile"]
                + " | awk -F\  '{print $1, $6, $9}'",
                shell=True)
            if len(res) > 0:
                appId = res.split(" ")[0]
                break
        print appId

        while self.finalState not in ("SUCCEEDED", "FAILED"):
            time.sleep(60)
            finalStatePrefix = subprocess.check_output(
                "ssh -o StrictHostKeyChecking=no -i /home/myntra/bianalytics.pem hadoop@" + self.config["master"]
                + " yarn application -status " + appId
                + "   | grep Final-State",
                shell=True)
            self.finalState = finalStatePrefix.split(":")[1].strip()
            print "status - " + self.finalState

    def complete(self):
        hostContent = self.s3handler.readFromS3(self.config["s3JobPath"] + self.config["hostsPath"])
        sz = len(hostContent.split("\n"))
        for i in range(sz - 1):
            hostExec = hostContent.split("\n")[i]
            hostLogin = "hadoop@" + hostExec
            print "removing /mnt/personify from " + hostLogin
            subprocess.call(
                ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/home/myntra/bianalytics.pem', hostLogin, 'sudo',
                 '-u',
                 'root', 'rm', '-r', '/mnt/personify/*'])

        print "deleting jar from Spark master..."
        masterLogin = "hadoop@" + self.config["master"]
        jarPath = self.config["jobDirectory"] + self.config["jar"]
        subprocess.call(
            ['ssh', '-o', 'StrictHostKeyChecking=no', '-i', '/home/myntra/bianalytics.pem',
             masterLogin, 'sudo', '-u', 'root', 'rm', '-r', jarPath]
        )

        if self.finalState == "SUCCEEDED":
            print "Spark Job Succeeded."
            print "Flow Succeeded."
            self.opts["startTime"] = self.config["endTime"]
            self.s3handler.writeToS3(self.config["defaultConfLocation"],
                                     " ".join([key + "=" + str(value) for key, value in self.opts.items()]))

        else:
            print "Job Failed."
            exit(1)
    def execute(self):
        super(PersonifyOrchestratorFlow, self).execute()


if __name__ == '__main__':
    import re
    import EmrLauncher

    defaultConf = "personify/defaultConfig.conf"
    s3Handler = S3()
    inp = re.split(r"\s+", s3Handler.readFromS3(defaultConf)) if len(sys.argv) < 2 \
        else sys.argv[1:]
    opts = {x.split("=")[0]: x.split("=")[1] for x in inp}
    print opts
    utils.checkInputArguments(opts)

    resource = EmrLauncher.createEphemeralCluster("Personify")
    clusterId = resource['JobFlowId']
    print "Cluster ID=%s" % clusterId
    EmrLauncher.waitForBootstrapping(clusterId)
    masterIp, allIps = EmrLauncher.getHostIPs(clusterId)
    opts["master"] = masterIp
    s3Handler.writeToS3(opts["s3JobPath"] + "hosts", "\n".join(allIps) + "\n")

    jobStartTime = currentJobTime = opts["startTime"]
    endTime = long(jobStartTime) + long(opts["jobInterval"])
    basePath = "/mnt/fabric/FabricOrchestration/"
    config = {
        "s3JobPath": opts["s3JobPath"],
        "downloadFileList": [
            ((opts["s3JobPath"] + opts["template"]), (basePath + opts["template"])),
            ((opts["s3JobPath"] + "launchFabricJob.sh"), basePath + "launchFabricJob.sh"),
            ((opts["s3JobPath"] + "copyAndLaunch.sh"), basePath + "copyAndLaunch.sh"),
            ((opts["s3JobPath"] + "hosts"), basePath + "hosts"),
            ((opts["s3JobPath"] + "launchCommand.sh"), basePath + "launchCommand.sh"),
            ((opts["s3JobPath"] + "spark-config.properties"), basePath + "spark-config.properties"),
            ((opts["s3JobPath"] + "fabric-0.1.1-SNAPSHOT-jar-with-dependencies.jar"), basePath + "fabric.jar"),
            ((opts["s3JobPath"] + "AppConfig.xsd"), basePath + "AppConfig.xsd")
        ],
        "xmlTemplatePath": basePath + opts["template"],
        "master": opts["master"],
        "jobStartTime": opts["startTime"],
        "sparkConfPath": "spark-config.properties",
        "copyScript": "copyAndLaunch.sh",
        "launchScript": "launchCommand.sh",
        "jar": "fabric.jar",
        "jobDirectory": "/home/hadoop/" + opts["startTime"] + "_" + opts["s3JobPath"],
        "hostsPath": "hosts",
        "basePath": basePath,
        "currentJobTime": currentJobTime,
        "launchFabricJob": "launchFabricJob.sh",
        "endTime": endTime,
        "defaultConfLocation": defaultConf
    }

    jobHandler = PersonifyOrchestratorFlow(config)
    jobHandler.setOpt(opts)
    jobHandler.execute()

    EmrLauncher.terminateCluster(clusterId)

